/////////////////////
//// INIT MODULE ////
/////////////////////
var InitModule = (function(Modernizr) {
    'use strict';


    ////////////////////////////
    //// VARIABLES PRIVADAS ////
    ////////////////////////////
    var vars = {},
        methods = {},
        viewToLoad = location.search.substring(1),
        mobileDetection,
        browserDetection,
        newState;




    ////////////////////////////
    //// VARIABLES GLOBALES ////
    ////////////////////////////
    vars = {
        isMobile: false,
        firefox: false,
        ie9: false,
        ie10: false,
        ie11: false
    };






    //////////////////////////
    //// METODOS PRIVADOS ////
    //////////////////////////
    mobileDetection = function() {
        if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1200px)')) {
            vars.isMobile = true;
        } else {
            vars.isMobile = false;
        }
    };


    browserDetection = function() {
        if (window.Detectizr.browser.name === 'ie') {
            if (window.Detectizr.browser.major === '9') {
                vars.ie9 = true;
            }
            if (window.Detectizr.browser.major === '10') {
                vars.ie10 = true;
            }
            if (window.Detectizr.browser.major === '11') {
                vars.ie11 = true;
            }
            if (window.Detectizr.browser.major < 10) {
                $('.update-browser').show(0);
                $('.main').hide(0);
            }
        }
    };






    //////////////////////////
    //// METODOS PUBLICOS ////
    //////////////////////////

    methods.ready = function() {

        //DETECTAR SI ES MOBILE
        mobileDetection();

        //DETECTAR NAVEGADORES
        browserDetection();


        //CARGA DE VISTAS (SOLO EN FRONTEND)
        if (viewToLoad.length > 0) {
            $('.panel-content').load(viewToLoad + '.html', function() {
                //INICIAR
                methods.init();
            });
        } else {
            //INICIAR
            methods.init();
        }
    };


    methods.init = function() {
        //NOTIFICACIÓN DE PRUEBA
        // methods.sendNotification({
        //   title:'Atención',
        //   message:'Se ha eliminado satisfactoriamente la alerta.'
        // });


        $(document).on('click', '.closex', function() {
            $('.detalle-agente').addClass('ocul');

        });

        // tabla dinamica
        $(document).ready(function() {
           $('.tooltip').tooltipster({
             position: 'bottom',
             offsetX: '-60px'
           });
        });
        $(document).on('click', '.btn_mostrar_panel', function() {
            $('.filtro_search').removeClass('hidden');
            $(this).addClass('hidden');
        });
        $(document).on('click', '.btn_ocultar_panel', function() {
            $('.filtro_search').addClass('hidden');
            $('.btn_mostrar_panel').removeClass('hidden');
        });


        $(function(){
          $('#TablaHistorial').tablesorter();
        });

         $(".spinner-time").TouchSpin({
          min: 1,
          max: 60,
          verticalbuttons: true,
          postfix: 'min',
          verticalupclass:'fa fa-angle-up',
          verticaldownclass: 'fa fa-angle-down'
        });
         $(".spinner-amount").TouchSpin({
          min: 1,
          max: 10,
          verticalbuttons: true,
          verticalupclass:'fa fa-angle-up',
          verticaldownclass: 'fa fa-angle-down'
        });

        $('.datepicker').datepicker();

        $('.tooltip-ott').tooltipster({
          position: 'bottom',
          theme: 'tooltipster-light',
	        maxWidth: 130,
          offsetX: 20,
          speed: 250,
          animation: 'grow'
        });

        $('.tooltip-ott-center').tooltipster({
          position: 'bottom',
          theme: 'tooltipster-light-center',
	        maxWidth: 130,
          speed: 250,
          animation: 'grow'
        });



        //INPUTS PERSONALIZADO (RADIO Y CHECKBOX)
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%'
        });

        setTimeout(function() {
          $('.multiselect').multiselect({
            nonSelectedText: 'Seleccione',
            allSelectedText: 'Todos'
          });
        }, 500);


        //SCROLL PESONALIZADO


        $('.scrollboxTwo').enscroll({
          showOnHover: false,
          verticalTrackClass: 'track3',
          verticalHandleClass: 'handle3'
        });

        $('.miniScrollOne').enscroll({
          showOnHover: false,
          verticalTrackClass: 'track3',
          verticalHandleClass: 'handle3'
        });

        $('.miniScrollTable').enscroll({
          showOnHover: false,
          verticalTrackClass: 'track3',
          verticalHandleClass: 'handle3'
        });

        $('.miniScrollTwo').enscroll({
          showOnHover: false,
          verticalTrackClass: 'track3',
          verticalHandleClass: 'handle3',

        });
        $('.scrollboxpop').enscroll({
            showOnHover: false,
            verticalTrackClass: 'track3',
            verticalHandleClass: 'handle3'
        });

        //ALTURA DINÁMICA DE CONTENEDORES
        methods.autosize();

        //BOTONES
        methods.eventsList();

        //IMPEDIR EFECTO ROLLOVER EN MÓVILES
        if (vars.isMobile) {
            $('.hover').removeClass('hover');
        }
    };


    methods.eventsList = function() {
        //FUNCIONES QUE SE DEBEN EJECUTAR EN EL RESIZE
        $(window).on('resize', methods.resizeActions);

        //EDITAR
        $(document).on('click', '.link-editar', function(e) {
            e.stopPropagation();
            $('.view-edit-registro').removeClass('hidden');
            $('.view-add-registro').addClass('hidden');
            $('.view-detail-vacio').addClass('hidden');
            $('.view-detail-solicitud').addClass('hidden');
        });

        //ELIMINAR
        $(document).on('click', '.link-eliminar', function(e) {
            e.stopPropagation();
            alert('Eliminar');
        });


        $(document).on('click', '.section-item', function() {
            $(this).siblings().addClass('section-item-disabled');
            $('.view-detail-solicitud').removeClass('hidden');
            $('.view-detail-vacio').addClass('hidden');
            $('.view-add-registro').addClass('hidden');
            $('.view-edit-registro').addClass('hidden');
            $(this).removeClass('section-item-disabled');
        });


        $(document).on('click', '.add-registro-link', function() {
            $('.view-add-registro').removeClass('hidden');
            $('.view-detail-vacio').addClass('hidden');
            $('.view-detail-solicitud').addClass('hidden');
            $('.view-edit-registro').addClass('hidden');
        });


        //CAMBIO DE ESTADO
        $(document).on('change', '#estado', function(){
          newState = $(this).children(":selected").val();
          $('.btn-estado').text(newState);
        });
    };


    methods.autosize = function() {
      // $('.view-detail').css('height', $(window).height() + 58);
      // $('.listado-item').css('height', $(window).height());

      // if ($(window).height() < 850) {
      //   $('.navbar-static-side').css('height', $('.listado-item').height() + 160);
      // }
    };


    methods.resizeActions = function() {

        methods.autosize();

        mobileDetection();

        browserDetection();
    };


    methods.sendNotification = function(data) {
        data.type = typeof data.type === 'undefined' ? 'success' : data.type;

        $.notify({
            title: data.title,
            message: data.message
        }, {
            type: data.type,
            delay: 5000,
            mouse_over: "pause",
            placement: {
                from: "bottom",
                align: "right"
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="title">{1}</span>' +
                '<span data-notify="message">{2}</span>' +
                '</div>'
        });
    };



    return {
        methods: methods,
        vars: vars
    };


})(Modernizr);





//CUANDO HA CARGADO EL DOM
$(document).ready(InitModule.methods.ready);
